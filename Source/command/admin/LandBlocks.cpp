#include <StdAfx.h>
#include "easylogging++.h"
#include "Client.h"
#include "WeenieObject.h"
#include "Player.h"
#include "World.h"
#include "WorldLandBlock.h"
#include "ChatMsgs.h"

#include "ClientCommands.h"

CLIENT_COMMAND(resetlb, "[lbid]", "Reset the current or specified landblock (in hex)", ADMIN_ACCESS, SERVER_CATEGORY)
{
	uint32_t lbid = 0;

	if (argc > 0)
	{
		lbid = strtoul(argv[0], NULL, 16);
	}
	else
	{
		lbid = (pPlayer->GetLandcell() & LandDefs::blockid_mask) >> LandDefs::block_part_shift;
	}

	SERVER_INFO << "Attempt to reset" << lbid;

	CWorldLandBlock *block = g_pWorld->GetLandblock((WORD)lbid);
	if (block)
	{
		block->RespawnNextTick();
		return false;
	}

	return true;
}
